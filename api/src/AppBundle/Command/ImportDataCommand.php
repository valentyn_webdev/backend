<?php


namespace AppBundle\Command;


use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Import products and categories from json file to the database
 */
class ImportDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:import-data')
            ->setDescription('Import data to the database from json file')
            ->addArgument('json', InputArgument::REQUIRED, 'Absolute path to json file with data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->isAlreadyImported()) {
            $output->writeln('Products have been already imported');
            return;
        }

        $output->writeln('Start import');

        $filePath = $input->getArgument('json');
        $data = json_decode(file_get_contents($filePath), true);

        $em = $this->getContainer()->get('doctrine')->getManager();

        $categories = [];

        foreach ($data['products'] as $productInfo) {
            if (!isset($categories[$productInfo['category']])) {
                $category = (new Category())->setName($productInfo['category']);
                $categories[$productInfo['category']] = $category;
                $em->persist($category);
            }

            $product = (new Product())
                ->setName($productInfo['name'])
                ->setCategory($categories[$productInfo['category']])
                ->setPrice($productInfo['price'])
                ->setQuantity($productInfo['quantity'])
                ->setSku($productInfo['sku']);

            $em->persist($product);
        }

        $em->flush();

        $output->writeln('Finish import');
    }

    /**
     * Check if products have already been imported
     * @return bool
     */
    private function isAlreadyImported()
    {
        return !is_null($this->getContainer()
            ->get('doctrine')
            ->getRepository(Product::class)
            ->findOneBy([]));
    }
}