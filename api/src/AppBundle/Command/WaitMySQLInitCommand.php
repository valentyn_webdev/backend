<?php


namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Wait till MySQL finishes initialization and starts listen for connections
 */
class WaitMySQLInitCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('app:mysql-wait')
            ->setDescription('Wait till MySQL finishes initialization and starts listen for connections');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
       $output->writeln('Wait for MySQL initialization');
        while (true) {
            try {
                $this->getContainer()->get('doctrine')->getManager()->getConnection()->connect();
                break;
            } catch  (\Doctrine\DBAL\Exception\ConnectionException $e) {
                sleep(10);
            }
        }

        $output->writeln('MySQL finished initialization!');

    }
}