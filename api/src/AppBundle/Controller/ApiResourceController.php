<?php


namespace AppBundle\Controller;


use AppBundle\Exception\ValidationFailedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Basic controller for other API controllers. Contains some useful logic to avoid code duplication
 */
abstract class ApiResourceController extends Controller
{
    protected const LIST_LIMIT = 100;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * ApiResourceController constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    /**
     * Get sorting direction from request parameters
     * @param Request $request
     * @return mixed
     */
    protected function getSortDirection(Request $request)
    {
        $sortDirection = $request->get('sortDirection', 'ASC');

        if ($sortDirection !== 'ASC' && $sortDirection !== 'DESC') {
            throw new BadRequestHttpException('Parameter sortDirection must be equal ASC or DESC');
        }

        return $sortDirection;
    }

    /**
     * Serialize data and get response instance
     * @param $data
     * @param array $serializerContext
     * @return JsonResponse
     */
    protected function getResponse($data, $serializerContext = [])
    {
        return JsonResponse::fromJsonString($this->serializer->serialize($data, 'json', $serializerContext));
    }

    /**
     * Get response for successful actions (for example successful DELETE)
     * @return JsonResponse
     */
    protected function getSuccessResponse()
    {
        return new JsonResponse([
            'success' => true
        ]);
    }

    /**
     * Get and parse JSON from request body
     * @param Request $request
     * @return mixed
     * @throws ValidationFailedException
     */
    protected function getRequestData(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (json_last_error() === JSON_ERROR_NONE) {
            return $data;
        } else {
            throw new ValidationFailedException('JSON is invalid');
        }
    }
}