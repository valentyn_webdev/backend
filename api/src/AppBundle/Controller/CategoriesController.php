<?php


namespace AppBundle\Controller;


use Swagger\Annotations as SWG;
use AppBundle\Entity\Category;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CategoriesController extends ApiResourceController
{
    /**
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the categories",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Category::class, groups={"basic"}))
     *     )
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="number",
     *     description="Offset"
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="number",
     *     description="Limit results"
     * )
     * @SWG\Parameter(
     *     name="sortDirection",
     *     in="query",
     *     type="string",
     *     description="Sort direction by column 'name': ASC or DESC"
     * )
     * @SWG\Tag(name="Categories")
     *
     * @Route("/categories", methods={"GET"})
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return JsonResponse
     */
    public function getListAction(SerializerInterface $serializer, Request $request)
    {
        $orderBy = ['name' => $this->getSortDirection($request)];
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', self::LIST_LIMIT);

        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findBy([], $orderBy , $limit, $offset);

        return $this->getResponse($categories, ['groups' => ['basic']]);
    }
}