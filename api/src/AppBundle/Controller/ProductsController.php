<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Exception\ValidatorException;
use AppBundle\Service\Products;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ProductsController extends ApiResourceController
{
    private const DEFAULT_SERIALIZER_CONTEXT = ['groups' => ['basic']];

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the products",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Product::class, groups={"basic"}))
     *     )
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="number",
     *     description="Offset"
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="number",
     *     description="Limit results"
     * )
     * @SWG\Parameter(
     *     name="sortDirection",
     *     in="query",
     *     type="string",
     *     description="Sort direction by column 'name': ASC or DESC"
     * )
     * @SWG\Tag(name="Products")
     *
     * @Route("/products", methods={"GET"})
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return JsonResponse
     */
    public function getListAction(SerializerInterface $serializer, Request $request)
    {
        $orderBy = ['name' => $this->getSortDirection($request)];
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', self::LIST_LIMIT);

        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findBy([], $orderBy, $limit, $offset);

        return $this->getResponse($products, self::DEFAULT_SERIALIZER_CONTEXT);
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the product by id",
     *     @Model(type=Product::class, groups={"basic"})
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="number",
     *     description="Product id"
     * )
     * @SWG\Tag(name="Products")
     * @Route("/products/{id}", methods={"GET"})
     * @ParamConverter(name="id", class="AppBundle\Entity\Product")
     * @param Product $product
     * @return JsonResponse
     */
    public function getAction(Product $product)
    {
        return $this->getResponse($product, self::DEFAULT_SERIALIZER_CONTEXT);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the created product",
     *     @Model(type=Product::class, groups={"basic"})
     * )
     * @SWG\Parameter(
     *         name="product",
     *         in="body",
     *         @SWG\Schema(type="object", ref="#/definitions/Product")
     * ),
     * @SWG\Tag(name="Products")
     * @Route("/products", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param Products $productsService
     * @return JsonResponse
     * @throws ValidatorException
     * @throws \AppBundle\Exception\ValidationFailedException
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function createAction(Request $request, Products $productsService)
    {
        $data = $this->getRequestData($request);

        $product = $productsService->save(new Product, $data['name'], $data['sku'], $data['quantity'], $data['price'], $data['category']);

        return $this->getResponse($product, self::DEFAULT_SERIALIZER_CONTEXT);
    }

    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated product",
     *     @Model(type=Product::class, groups={"basic"})
     * )
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         @SWG\Schema(type="object", ref="#/definitions/Product")
     *     ),
     * @SWG\Tag(name="Products")
     * @Route("/products/{id}", methods={"PUT"})
     * @ParamConverter(name="id", class="AppBundle\Entity\Product")
     * @IsGranted("ROLE_USER")
     * @param Product $product
     * @param Products $productsService
     * @param Request $request
     * @return JsonResponse
     * @throws ValidatorException
     * @throws \AppBundle\Exception\ValidationFailedException
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function updateAction(Product $product, Products $productsService, Request $request)
    {
        $data = $this->getRequestData($request);

        $product = $productsService->save($product, $data['name'], $data['sku'], $data['quantity'], $data['price'], $data['category']);

        return $this->getResponse($product);
    }

    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Delete the product by id"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="number",
     *     description="Product id"
     * )
     * @SWG\Tag(name="Products")
     * @Route("/products/{id}", methods={"DELETE"})
     * @ParamConverter(name="id", class="AppBundle\Entity\Product")
     * @IsGranted("ROLE_USER")
     * @param Product $product
     * @param Products $productsService
     * @return JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction(Product $product, Products $productsService)
    {
        $productsService->delete($product);

        return $this->getSuccessResponse();
    }

}