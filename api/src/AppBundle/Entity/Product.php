<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="product")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Product
{
    /**
     * @var int
     *
     * @Groups("basic")
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Groups("basic")
     *
     * @Assert\NotBlank(message="Name of product should not be blank")
     * @Assert\Length(
     *     min = 3,
     *     max = 255,
     *     minMessage="Name of product should be longer than 3 symbols",
     *     maxMessage="Name of product should be shorter than 255 symbols",
     * )
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Groups("basic")
     *
     * @Assert\NotBlank(message="SKU should not be blank")
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage="SKU should be longer than 1 symbols",
     *     maxMessage="SKU should be shorter than 255 symbols",
     * )
     *
     * @ORM\Column(name="sku", type="string", length=255, nullable=false)
     */
    private $sku;

    /**
     * @var float
     *
     * @Groups("basic")
     *
     * @Assert\Range(
     *     min = 0,
     *     minMessage="Price should be greater than 0",
     *     max = 10000000,
     *     maxMessage="Price should be less than 10000000"
     * )
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var int
     *
     * @Groups("basic")
     *
     * @Assert\GreaterThanOrEqual(value="1", message="Quantity should be greater than 1")
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false, options={"unsigned": true})
     */
    private $quantity;


    /**
     * @var Category
     *
     * @Groups("basic")
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category", fetch="EAGER")
     */
    private $category;

    /**
     * @var \DateTime
     *
     * @Groups("basic")
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Groups("basic")
     *
     * @ORM\Column(name="modified_at", type="datetime")
     */
    private $modifiedAt;

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt(): \DateTime
    {
        return $this->modifiedAt;
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Product
     */
    public function setName(string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     * @return Product
     */
    public function setSku(string $sku): Product
    {
        $this->sku = $sku;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Product
     */
    public function setPrice(float $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Product
     */
    public function setQuantity(int $quantity): Product
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Product
     */
    public function setCategory(Category $category): Product
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate()
     */
    public function setModifiedAtValue()
    {
        $this->modifiedAt = new \DateTime();
    }
}