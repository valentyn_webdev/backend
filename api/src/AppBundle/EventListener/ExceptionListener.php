<?php

namespace AppBundle\EventListener;

use AppBundle\Exception\ValidationFailedException;
use Doctrine\ORM\EntityNotFoundException;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * Intercepts exception and converts it to json response
 * @package AppBundle\EventListener
 */
class ExceptionListener
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof BadCredentialsException) {
            $code = JsonResponse::HTTP_UNAUTHORIZED;
            $message = 'Wrong username or password.';
        } else if ($exception instanceof EntityNotFoundException)  {
            $code = 404;
            $message = 'Not found.';
        }  else if ($exception instanceof ValidationFailedException) {
            $code = JsonResponse::HTTP_BAD_REQUEST;
            $message = $exception->getMessage();
        } else if ($exception instanceof NotFoundHttpException) {
            $code = $exception->getStatusCode();
            $message = 'Not found.';
        } else if ($exception instanceof HttpExceptionInterface) {
            $code = $exception->getStatusCode();
            $message = $exception->getMessage();
        } else {
            $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
            $message = 'Oops.. Error happened. Please try one more time.';
        }

        $this->logger->error($exception->getCode() . ' ' . $exception->getMessage() . ' ' . $exception->getTraceAsString());

        $resp = new JsonResponse([
            'code' => $code,
            'message' => is_array($message) ? $message : [$message]
        ], $code);

        $event->setResponse($resp);

    }
}