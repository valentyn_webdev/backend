<?php


namespace AppBundle\Exception;

/**
 * Use to throw validation error. Interceptor will catch it and show error message
 */
class ValidationFailedException extends \Exception
{

}