<?php

namespace AppBundle\Exception;


use Exception;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Use to throw errors that validator returned. Interceptor will catch them and show error message
 */
class ValidatorException extends ValidationFailedException
{
    private $errors = [];
    public function __construct(ConstraintViolationListInterface $errors, $code = 0, Exception $previous = null)
    {
        for ($i = 0; $i < $errors->count(); $i++) {
            $this->errors[] = $errors->get($i)->getMessage();
        }
        parent::__construct(implode('. ', $this->errors), $code, $previous);
    }
}