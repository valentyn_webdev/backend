<?php


namespace AppBundle\Service;


use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Exception\ValidatorException;
use Doctrine\ORM\EntityNotFoundException;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Validates, saves and deletes products
 */
class Products
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RegistryInterface
     */
    private $doctrine;

    /**
     * Products constructor.
     * @param ValidatorInterface $validator
     * @param LoggerInterface $logger
     * @param RegistryInterface $doctrine
     */
    public function __construct(ValidatorInterface $validator, LoggerInterface $logger, RegistryInterface $doctrine)
    {
        $this->validator = $validator;
        $this->logger = $logger;
        $this->doctrine = $doctrine;
    }

    /**
     * Update product with new data and save it to the database
     * @param Product $product
     * @param $name
     * @param $sku
     * @param $quantity
     * @param $price
     * @param $categoryId
     * @return Product
     * @throws EntityNotFoundException
     * @throws ValidatorException
     */
    public function save(Product $product, $name, $sku, $quantity, $price, $categoryId)
    {
        // Set new data to a product

        /** @var Category $category */
        $category = $this->doctrine
            ->getRepository(Category::class)
            ->find($categoryId);

        if (!$category) {
            throw new EntityNotFoundException();
        }

        $product
            ->setName((string) $name)
            ->setSku((string) $sku)
            ->setQuantity((float) $quantity)
            ->setPrice((float) $price)
            ->setCategory($category);

        // Validate product
        $errors = $this->validator->validate($product);

        // Save product
        if (!count($errors)) {
            $this->doctrine->getManager()->persist($product);
            $this->doctrine->getManager()->flush();

            $this->logger->info("Product #{$product->getId()} was successfully saved");
        } else {
            throw new ValidatorException($errors);
        }

        return $product;
    }

    /**
     * Delete product from database
     * @param Product $product
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Product $product)
    {
        $productId = $product->getId();
        $this->doctrine->getEntityManager()->remove($product);
        $this->doctrine->getEntityManager()->flush();

        $this->logger->info("Product #$productId was successfully deleted");
    }
}