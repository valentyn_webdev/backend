#!/usr/bin/env bash

env=prod

mkdir -p api/var/cache
mkdir -p api/var/logs
mkdir -p api/var/sessions

docker-compose build
docker-compose up -d

docker-compose exec api composer install --no-interaction

docker-compose exec api bin/console cache:clear --env=$env

docker-compose exec api bin/console app:mysql-wait  --env=$env

docker-compose exec api bin/console doctrine:schema:update --force  --env=$env

docker-compose exec api bin/console app:import-data data-fixtures.json --env=$env

# Need for Swagger (docs)
docker-compose exec api php bin/console assets:install --symlink --env=$env

sudo chmod -R 777 api/var/cache
sudo chmod -R 777 api/var/logs
sudo chmod -R 777 api/var/sessions
